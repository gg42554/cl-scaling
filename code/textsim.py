from __future__ import division
import numpy as np
from scipy import spatial
import nltk
from munkres import Munkres, print_matrix
from sys import stdin
import math
from inf_content import InformationContent
import copy

# Class implementing different semantic similarity metrics: alignment similarity and aggregation similarity
class Semsim(object):
	"""description of class"""

	# Class for computing simple unsupervised STS scores: Aggreg, Optimal alignment, and Greedy alignment
	def __init__(self, stopwords, inf_cont):
		self.stopwords = stopwords
		self.punctuation = [".", ",", "!", ":", "?", ";", "-", ")", "(", "[", "]", "{", "}", "...", "/", "\\", u"``", "''", "\"", "'"]
		self.ic = inf_cont
	
	# Filter out only the eligible tokens from the input texts (no punctuation and no stopwords)
	def select_eligible_tokens(self, src_lang_sent, trg_lang_sent, src_lang = 'en', trg_lang = 'en'):
		src_tokens = [x.strip().lower() for x in nltk.word_tokenize(src_lang_sent) if len(x.strip()) > 1 and x.strip().lower() not in self.stopwords[src_lang] and x.strip().lower() not in self.punctuation]
		trg_tokens = [x.strip().lower() for x in nltk.word_tokenize(trg_lang_sent) if len(x.strip()) > 1 and x.strip().lower() not in self.stopwords[trg_lang] and x.strip().lower() not in self.punctuation]

		for i in range(len(trg_tokens)):
			pcs = [p for p in self.punctuation if trg_tokens[i].endswith(p)]
			if (len(pcs) > 0):
				trg_tokens[i] = trg_tokens[i][:-1]
		return [src_tokens, trg_tokens]

	def tokenize_clean(self, text, lang):
		toks = [x.strip().lower() for x in nltk.word_tokenize(text) if len(x.strip()) > 1 and x.strip().lower() not in self.stopwords[lang] and x.strip().lower() not in self.punctuation]
		return toks
	
	def greedy_alignment_similarity(self, source_freq_dictionary, target_freq_dictionary, embedding_similarity_function, src_lang = 'en', trg_lang = 'en'):
		print("Greedy semantic alignment: start...")

		src_dict = copy.deepcopy(source_freq_dictionary)
		trg_dict = copy.deepcopy(target_freq_dictionary)

		src_voc = list(src_dict.keys())
		src_voc.sort()
		trg_voc = list(trg_dict.keys())
		trg_voc.sort()

		# similarity matrix computation
		print("Greedy semantic alignment: computing embedding similarities...")
		sim_matrix = []
		for i in range (len(src_voc)):
			row = []
			for j in range (len(trg_voc)):
				sim = embedding_similarity_function(src_voc[i], trg_voc[j], src_lang, trg_lang)
				row.append(sim)
			sim_matrix.append(row)		

		# greedy alignment
		matrix = np.matrix(sim_matrix)
				
		greedy_align_sum = 0
		num_align = 0 

		print("Greedy semantic alignment: aligning...")
		while matrix.shape[0] > 0 and matrix.shape[1] > 0: 
			ind = np.argmax(matrix)
			ind_src = ind // matrix.shape[1]
			ind_trg =  ind % matrix.shape[1]

			first_word = src_voc[ind_src]
			second_word = trg_voc[ind_trg]

			mincnt = min(src_dict[first_word], trg_dict[second_word])
			similarity = matrix[ind_src, ind_trg]
			greedy_align_sum += matrix[ind_src, ind_trg] * (float(mincnt)) #* min(ic_first, ic_second)
			num_align += mincnt

			if matrix.shape[0] == 1 or matrix.shape[1] == 1:
				break			
			else:
				src_dict[first_word] = src_dict[first_word] - mincnt
				if src_dict[first_word] == 0:
					matrix = matrix = np.delete(matrix, ind_src, 0)
					del src_voc[ind_src]
					
				trg_dict[second_word] = trg_dict[second_word] - mincnt
				if trg_dict[second_word] == 0:
					matrix = matrix = np.delete(matrix, ind_trg, 1)
					del trg_voc[ind_trg]

		prec = greedy_align_sum / (num_align + min(sum(src_dict.values()), sum(trg_dict.values())))
		rec = greedy_align_sum / (num_align + max(sum(src_dict.values()), sum(trg_dict.values())))
		s1 = (prec + rec)/2
		
		s2 = greedy_align_sum / num_align
		s3 = greedy_align_sum / (1.0*(sum(source_freq_dictionary.values()) + sum(target_freq_dictionary.values())))
		
		return (s1, s2, s3)

	
	# Computes the aggregation similarity for a given pair of texts
	def aggregation_similarity(self, first_freq_dict, second_freq_dict, embeddings, first_lang = 'en', second_lang = 'en'):
		first_agg_vec = self.aggregate_vector(first_freq_dict, embeddings, first_lang)
		second_agg_vec = self.aggregate_vector(second_freq_dict, embeddings, second_lang)
		return np.dot(first_agg_vec, second_agg_vec)

	def aggregate_vector(self, freq_dict, embeddings, lang = 'en'):
		agg_vec = np.zeros((len(embeddings.lang_embeddings['en']['dog'])))
		for w in freq_dict:
			if w in embeddings.lang_embeddings[lang] or w.lower() in embeddings.lang_embeddings[lang]:
				emb = embeddings.lang_embeddings[lang][w] if w in embeddings.lang_embeddings[lang] else embeddings.lang_embeddings[lang][w.lower()]
				norm = embeddings.lang_emb_norms[lang][w] if w in embeddings.lang_emb_norms[lang] else embeddings.lang_emb_norms[lang][w.lower()]
				if (self.ic is None):
					agg_vec = agg_vec + np.multiply(freq_dict[w] * 1.0 / norm, emb) #np.multiply(1.0/norm, emb)
				else:
					agg_vec = agg_vec + np.multiply(freq_dict[w] * self.ic.value(w.lower()), emb) #np.multiply(1.0/norm, emb)
		agg_vec = np.multiply(1.0 / np.linalg.norm(agg_vec, 2), agg_vec)
		return agg_vec

	def docsim_direct_align(self, src_doc, trg_doc, sent_similarity_function, embedding_similarity_function, src_lang = 'en', trg_lang = 'en'):
		return sent_similarity_function(src_doc, trg_doc, embedding_similarity_function, src_lang, trg_lang)


