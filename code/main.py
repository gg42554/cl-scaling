import io_helper
from corpus import Corpus
import corpus
from scaler import WordfishScaler
from scaler import LinearScaler
from graph import Graph
import numpy as np
from embeddings import Embeddings
from transmat import Transmat
from sys import stdin
from textsim import Semsim
from textsim import Termsim
from inf_content import InformationContent
from evaluation import Evaluator
import itertools

# Example code showing how to use the functionality needed for cross-lingual text scaling

if __name__ == '__main__':

	# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	# COMPUTING SIMILARITIES 
	# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	# loading monolingual embeddings

	embeddings = Embeddings()
	embeddings.load_embeddings('data/embeddings/english.txt', 200, 200000, pickled = False, language = 'en')
	embeddings.load_embeddings('data/embeddings/italian.txt', 300, 200000, pickled = False, language = 'it')
	embeddings.load_embeddings('data/embeddings/spanish.txt', 300, 200000, pickled = False, language = 'es')
	embeddings.load_embeddings('data/embeddings/french.txt', 300, 200000, pickled = False, language = 'fr')
	embeddings.load_embeddings('data/embeddings/german.txt', 300, 200000, pickled = False, language = 'de')

	# learning translation matrices and creating multilingual embedding space
	
	train_set = io_helper.load_translation_pairs('data/trans-pairs/de-en-train-4k.csv')
	transmat = Transmat(embeddings, 'de', 'en')
	transmat.train(train_set)
	transmat.translate_all()
	
	train_set = io_helper.load_translation_pairs('data/trans-pairs/es-en-train-4k.csv')
	transmat = Transmat(embeddings, 'es', 'en')
	transmat.train(train_set)
	transmat.translate_all()

	train_set = io_helper.load_translation_pairs('data/trans-pairs/it-en-train-4k.csv')
	transmat = Transmat(embeddings, 'it', 'en')
	transmat.train(train_set)
	transmat.translate_all()	

	train_set = io_helper.load_translation_pairs('data/trans-pairs/fr-en-train-4k.csv')
	transmat = Transmat(embeddings, 'fr', 'en')
	transmat.train(train_set)
	transmat.translate_all()

	# loading stopwords for each of the languages
	
	stopwords_en = io_helper.load_file_lines("data/stopwords/stopwords-en.txt")
	stopwords_es = io_helper.load_file_lines("data/stopwords/stopwords-es.txt")
	stopwords_de = io_helper.load_file_lines("data/stopwords/stopwords-de.txt")
	stopwords_it = io_helper.load_file_lines("data/stopwords/stopwords-it.txt")
	stopwords_fr = io_helper.load_file_lines("data/stopwords/stopwords-fr.txt")

	stopwords = { 'en' : stopwords_en, 'es' : stopwords_es, 'de' : stopwords_de, 'fr' : stopwords_fr, 'it' : stopwords_it }
	similaritizer = Semsim(stopwords, inf_cont = None)

	# loading and preprocessing documents documents
	
	#docs = io_helper.load_corpus("data/corpus/monolingual")
	docs = io_helper.load_corpus("data/corpus/multilingual")
	
	corp = Corpus(docs)	
	corp.tokenize(stopwords = stopwords)
	corp.build_occurrences()

	# computing  semantic similarities between all pairs of documents in the corpus
	
	corpus.compute_semantic_similarities(similaritizer.docsim_direct_align, similaritizer.greedy_alignment_similarity, embeddings.word_similarity)
	corpus.compute_semantic_similarities_aggregation(similaritizer.aggregation_similarity, embeddings)
	
	wf_scaler = WordfishScaler(corp)
	wf_scaler.initialize()
	wf_scaler.train(learning_rate = 0.0005, num_iters = 10000)
	
	# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	# SCALING and EVALUATION 
	# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	# loading similarities (produced by any method) and evaluating performance against gold standard

	sc_opts = [True, False]
	sim_lists = ["pairwise-similarities/monoling-align.txt", "pairwise-similarities/crossling-align.txt", "pairwise-similarities/monoling-agg.txt", "pairwise-similarities/crossling-agg.txt"]
	alphas = [0.0, 0.1, 0.15, 0.2, 0.4]
	sim_score_indices = [2,3,4]

	gold_path = "data/gold-standard/lr-ideology.txt"
	#gold_path = "data/gold-standard/eu-intergation.txt"	

	gold = io_helper.load_scores(gold_path)
	wordfish = io_helper.load_scores("wordfish_scores.txt")

	ev = Evaluator(gold, wordfish)
	acc_wf = ev.pairwise_accuracy()
	pearson_wf, spearman_wf = ev.correlation_scores()
	pearson_dist_wf, spearman_dist_wf = ev.correlation_distances()

	test_settings = itertools.product(sim_lists, sim_score_indices, sc_opts, alphas)
	
	for test in test_settings:
		print("TEST SETTING: " + str(test)) 		
		sims_path = test[0]
		sim_score_index = test[1]
		same_country = test[2]
		alpha = test[3]

		if "-agg.txt" in sims_path and sim_score_index != 2:
			continue

		sims = io_helper.load_similarity_scores(sims_path, score_index = sim_score_index, same_country = same_country)  
		
		min_sim = min([x[2] for x in sims])
		max_sim = max([x[2] for x in sims])
		sims = [(x[0], x[1], (x[2] - min_sim) / (max_sim - min_sim)) for x in sims]
	
		nodes = []
		nodes.extend([x[0] for x in sims])
		nodes.extend([x[1] for x in sims])
		nodes = list(set(nodes))

		vec = corpus.most_dissimilar_vector(nodes, sims)
		fixed = {vec.index(x): x for x in vec if x == -1 or x == 1}

		graph = Graph(nodes = nodes, edges = sims, symmetric = True)
		
		res = graph.pagerank(alpha = alpha, rescale_extremes = True) 
		evaluator = Evaluator(gold, res)
		acc = evaluator.pairwise_accuracy()
		prho, srho = evaluator.correlation_scores()
		print("PageRank:  " + str(acc) + ";" + str(prho) + "; " + str(srho))
		
		res = graph.harmonic_function_label_propagation([(vec.index(x), x) for x in vec if x == -1 or x == 1], rescale_extremes = True)
		evaluator = Evaluator(gold, res)
		acc = evaluator.pairwise_accuracy()
		prho, srho = evaluator.correlation_scores()
		print("PageRank:  " + str(acc) + ";" + str(prho) + "; " + str(srho))
		
	print("Best: " + str(maximum) + str(best_setting))