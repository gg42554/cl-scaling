# various useful I/O methods

from __future__ import division
import codecs
from os import listdir
from os.path import isfile, join
import pickle
import numpy as np

def load_file_lines(filepath):
    return [x.strip() for x in list(codecs.open(filepath, "r", "utf-8").readlines())]

def read_file_to_end(filepath):
	return codecs.open(filepath, "r", "utf-8").read()

def load_corpus(dirpath):
	files = [f for f in listdir(dirpath) if isfile(join(dirpath, f))]
	return [(f, read_file_to_end(join(dirpath, f))) for f in files]

def load_embeddings_dict(filepath, limit = 500000):
    dict = {}
    cnt = 0
    with codecs.open(filepath,'r',encoding='utf8', errors='replace') as f:
        for line in f:
            try:
                cnt += 1
                if limit and cnt > limit: 
                    break
                if (cnt % 1000 == 0): 
                    print("Loading embeddings: " + str(cnt))
                if cnt > 1:
                    splt = line.split()
                    word = splt[0]
                    vec = [float(x) for x in splt[1:]]
                    dict[word] = vec
            except (IndexError, UnicodeEncodeError):
                print("Incorrect format line!")
    return dict

def load_embeddings_dict_with_norms(filepath, limit = 500000):
    dict = {}
    norms = {}
    cnt = 0
    with codecs.open(filepath,'r',encoding='utf8', errors='replace') as f:
        for line in f:
            try:
                cnt += 1
                if limit and cnt > limit: 
                    break
                if (cnt % 1000 == 0): 
                    print("Loading embeddings: " + str(cnt))
                if cnt > 1:
                    splt = line.split()
                    word = splt[0]
                    vec = [float(x) for x in splt[1:]]
                    dict[word] = vec
                    norms[word] = np.linalg.norm(vec, 2)
            except (IndexError, UnicodeEncodeError):
                print("Incorrect format line!")
    return dict, norms

def deserialize_embeddings_dict(filepath):
    dict = pickle.load(open(filepath, 'rb'))
    return dict

def deserialize_embeddings_dict_with_norms(filepath):
	dict = pickle.load(open(filepath, 'rb'))
	norms = {}
	for k in dict:
		norms[k] = np.linalg.norm(dict[k], 2)
	return dict, norms

def load_translation_pairs(filepath):
	lines = list(codecs.open(filepath,'r',encoding='utf8', errors='replace').readlines())
	dataset = []; 
	for line in lines:
		spl = line.split(',')
		srcword = spl[0].strip()
		trgword = spl[1].strip(); 
		if (" " not in srcword.strip()) and  (" " not in trgword.strip()):
			dataset.append((srcword, trgword)); 
	return dataset	

def load_scores(filepath):
	lines = list(codecs.open(filepath,'r',encoding='utf8', errors='replace').readlines())
	return { l.split()[0].strip() : float(l.split()[1].strip()) for l in lines}

def load_similarity_scores(filepath, score_index = 2, same_country = True):
	lines = list(codecs.open(filepath,'r',encoding='utf8', errors='replace').readlines())
	sims = [(l.split()[0].strip(), l.split()[1].strip(), float(l.split()[score_index].strip())) for l in lines]
	if not same_country:
		sims = [s for s in sims if s[0][0] != s[1][0]]
	return sims