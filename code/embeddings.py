import numpy as np
import io_helper
from scipy import spatial

# Class that loads and compares word embeddings. Supports parallel loading of embeddings in multiple languages
class Embeddings(object):
	"""description of class"""
	def __init__(self):
		self.lang_embeddings = {}
		self.lang_emb_norms = {}
		self.emb_sizes = {}
		self.cache = {}

	def load_embeddings(self, filepath, num_dims, limit, pickled = False, language = 'en'):
		dict, norms = io_helper.deserialize_embeddings_dict_with_norms(filepath) if pickled else io_helper.load_embeddings_dict_with_norms(filepath, limit = limit)
		if language in self.lang_embeddings:
			self.lang_embeddings[language].update(dict)
			self.lang_embs_norms[language].update(norms)
			self.emb_sizes[language] = num_dims
		else:
			self.lang_embeddings[language] = dict
			self.lang_emb_norms[language] = norms
			self.emb_sizes[language] = num_dims

	def word_similarity(self, first_word, second_word, first_language = 'en', second_language = 'en'):
		cache_str = min(first_word, second_word) + "-" + max(first_word, second_word)
		if (first_language + "-" + second_language) in self.cache and cache_str in self.cache[first_language + "-" + second_language]:
			return self.cache[first_language + "-" + second_language][cache_str]
		elif (first_word not in self.lang_embeddings[first_language] and first_word.lower() not in self.lang_embeddings[first_language]) or (second_word not in self.lang_embeddings[second_language] and second_word.lower() not in self.lang_embeddings[second_language]):
			if ((first_word in second_word or second_word in first_word) and first_language == second_language) or first_word == second_word:
				 return 1
			else:
				 return 0
		first_emb = self.lang_embeddings[first_language][first_word] if first_word in self.lang_embeddings[first_language] else self.lang_embeddings[first_language][first_word.lower()]
		second_emb = self.lang_embeddings[second_language][second_word] if second_word in self.lang_embeddings[second_language] else self.lang_embeddings[second_language][second_word.lower()]

		first_norm = self.lang_emb_norms[first_language][first_word] if first_word in self.lang_emb_norms[first_language] else self.lang_emb_norms[first_language][first_word.lower()]
		second_norm = self.lang_emb_norms[second_language][second_word] if second_word in self.lang_emb_norms[second_language] else self.lang_emb_norms[second_language][second_word.lower()]

		score =  np.dot(first_emb, second_emb) / (first_norm * second_norm) #1 - spatial.distance.cosine(first_emb, second_emb)
		if (first_language + "-" + second_language) not in self.cache:
			self.cache[first_language + "-" + second_language] = {}
			if cache_str not in self.cache[first_language + "-" + second_language]:
				self.cache[first_language + "-" + second_language][cache_str] = score		
		return score

	def aggregate_sentence_embedding(self, sentence_tokens, language = 'en'):
		vec_src = np.zeros(self.emb_sizes[language])
		cnt = 0
		for tok in sentence_tokens:
			if tok in self.lang_embeddings[language]:
				vec_src += self.lang_embeddings[language][tok]	
				cnt += 1
			elif tok.lower() in self.lang_embeddings[language]:
				vec_src += self.lang_embeddings[language][tok.lower()]
				cnt += 1
		if cnt == 0:
			 return vec_src
		else: 
			return np.multiply(1.0 / (float(cnt)), vec_src)	



