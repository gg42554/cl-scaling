import sys
import io_helper
import scipy
import math

# Normalizes sets of positions and evaluates the performance
class Evaluator(object):
	"""description of class"""
	def __init__(self, gold_dict, pred_dict):
		self.gold = gold_dict
		self.pred = pred_dict

		self.normalize_dict(self.gold)
		self.normalize_dict(self.pred)
		self.pred_inverted = {}
		for k in self.pred:
			self.pred_inverted[k] = -1.0 * self.pred[k]

		self.gold_sorted_keys = []
		self.pred_sorted_keys = []
		self.pred_inv_sorted_keys = []
		
		keys = list(self.gold.keys())
		keys.sort()
		for k in keys:
			self.gold_sorted_keys.append((k, self.gold[k]))
			self.pred_sorted_keys.append((k, self.pred[k]))
			self.pred_inv_sorted_keys.append((k, self.pred_inverted[k]))

		self.gold_distances = []
		self.pred_distances = []
		self.pred_inv_distances = []
		for i in range(len(keys) - 1):
			for j in range(i+1, len(keys)):
				dist_gold = math.fabs(self.gold[keys[i]] - self.gold[keys[j]])
				self.gold_distances.append(dist_gold)
				dist_pred = math.fabs(self.pred[keys[i]] - self.pred[keys[j]])
				self.pred_distances.append(dist_pred)
				dist_pred_inv = math.fabs(self.pred_inverted[keys[i]] - self.pred_inverted[keys[j]])
				self.pred_inv_distances.append(dist_pred_inv)
				
	# Normalizes the dictionary values to the [0, 1] range	
	def normalize_dict(self, dict):
		min_val = min(dict.values())		
		max_val = max(dict.values())
		for k in dict:
			dict[k] = 2.0*((dict[k] - min_val) / (max_val - min_val) - 0.5)		
		
	def pairwise_accuracy_single_dict(self, pred_dict):
		keys = list(self.gold.keys())
		count_good = 0.0
		count_all = 0.0
		for i in range(len(keys) - 1):
			for j in range(i+1, len(keys)):
				count_all += 1.0
				diff_gold = self.gold[keys[i]] - self.gold[keys[j]]
				diff_pred = pred_dict[keys[i]] - pred_dict[keys[j]]
				if (diff_gold < 0 and diff_pred < 0) or (diff_gold > 0 and diff_pred > 0):
					count_good += 1.0
		return count_good / count_all

	# Computes the pairwise accurary of the predicted positions with respect to gold positions	
	def pairwise_accuracy(self):	
		acc_pred = self.pairwise_accuracy_single_dict(self.pred)
		acc_pred_inv = self.pairwise_accuracy_single_dict(self.pred_inverted)
		return max(acc_pred, acc_pred_inv)

	def correlation_scores_single_dict(self, dict_sorted_keys):
		gold_vals = [x[1] for x in self.gold_sorted_keys]
		pred_vals = [x[1] for x in dict_sorted_keys]
		pearson = scipy.stats.pearsonr(gold_vals, pred_vals)
		spearman = scipy.stats.spearmanr(gold_vals, pred_vals)
		return [pearson, spearman]

	# Computes the correlations (Pearson and Spearman) between the predicted positions and gold positions	
	def correlation_scores(self):
		pearson_pred, spearman_pred = self.correlation_scores_single_dict(self.pred_sorted_keys)
		pearson_pred_inv, spearman_pred_inv = self.correlation_scores_single_dict(self.pred_inv_sorted_keys)
		return [max(pearson_pred[0], pearson_pred_inv[0]), max(spearman_pred[0], spearman_pred_inv[0])]