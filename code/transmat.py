from __future__ import division
import numpy as np
from embeddings import Embeddings
from scipy import spatial

# Class encompassing the translation matrix functionality
class Transmat(object):
	"""description of class"""
	# Constructor. initializes the instance with 
	# 1, the object containing both monolingual embeddings,2. source language and 3. target language
	def __init__(self, embeddings, first_lang, second_lang):
		self.embeddings = embeddings
		self.first_language = first_lang
		self.second_language = second_lang
	
	# learns the translation matrix from the provided set of word translation pairs
	def train(self, pairs): 
		embs_first = []
		embs_second = [] 
		for i in range(len(pairs)):
			first_word = pairs[i][0]
			second_word = pairs[i][1]
			if (first_word in self.embeddings.lang_embeddings[self.first_language] or first_word.lower() in self.embeddings.lang_embeddings[self.first_language]) and (second_word in self.embeddings.lang_embeddings[self.second_language] or second_word.lower() in self.embeddings.lang_embeddings[self.second_language]):
				first_emb = self.embeddings.lang_embeddings[self.first_language][first_word] if first_word in self.embeddings.lang_embeddings[self.first_language] else self.embeddings.lang_embeddings[self.first_language][first_word.lower()]
				embs_first.append(first_emb)
				second_emb = self.embeddings.lang_embeddings[self.second_language][second_word] if second_word in self.embeddings.lang_embeddings[self.second_language] else self.embeddings.lang_embeddings[self.second_language][second_word.lower()]
				embs_second.append(second_emb)
		self.source_mat = np.array(embs_first)
		self.target_mat = np.array(embs_second)
		self.matrix = np.dot(np.linalg.pinv(self.source_mat), self.target_mat)

	# evaluates the translation performance on the provided test set of word translation pairs	
	def test(self, pairs):
		p1 = 0; p5 = 0; counted = 0	
		for i in range(len(pairs)):
			first_word = pairs[i][0]
			second_word = pairs[i][1]
			if (first_word in self.embeddings.lang_embeddings[self.first_language] or first_word.lower() in self.embeddings.lang_embeddings[self.first_language]) and (second_word in self.embeddings.lang_embeddings[self.second_language] or second_word.lower() in self.embeddings.lang_embeddings[self.second_language]):
				counted += 1
				first_emb = self.embeddings.lang_embeddings[self.first_language][first_word] if first_word in self.embeddings.lang_embeddings[self.first_language] else self.embeddings.lang_embeddings[self.first_language][first_word.lower()]
				first_emb_trans = np.dot(first_emb, self.matrix)
				ms = self.most_similar(first_emb_trans, self.embeddings.lang_embeddings[self.second_language], self.embeddings.lang_emb_norms[self.second_language], 5)
				print("(" + str(i) + "/" + str(len(pairs)) + ") Word: " + str(first_word.encode(encoding='UTF-8', errors='ignore')) + "; Translation: " + str(second_word.encode(encoding='UTF-8', errors='ignore')))
				print("Most similar: " + str((str(ms)).encode(encoding='UTF-8', errors='ignore')))
				if second_word in ms or second_word.lower() in ms: 
					p5 += 1
					print("P5 match!")
				if second_word == ms[0] or second_word.lower() == ms[0]:
					p1 += 1
					print("P1 match!")
		return [p1 / counted, p5 / counted]
	
	# finds the most similar target words for the given source word, using the translation matrix to map
	def most_similar(self, embedding, emb_dict, emb_dict_norms, num):
		ms = []
		for w in emb_dict:
			if len(embedding) != len(emb_dict[w]):
				print("Unaligned embedding length: " + w)
			else:
				nrm = np.linalg.norm(embedding, 2)
				sim = np.dot(embedding, emb_dict[w]) / (nrm * emb_dict_norms[w]) #1 - spatial.distance.cosine(embedding, emb_dict[w])
				if (len(ms) < num) or (sim > ms[-1][1]):
					ms.append((w, sim))
					ms.sort(key = lambda x: x[1], reverse = True)
					if len(ms) > num: 
						ms.pop() 
		return [ws[0] for ws in ms]

	# Translates the entire source vocabulary from the source embedding space to the target embedding space, using the learnt translation matrix
	def translate_all(self):
		print("Translating vocabulary: " + self.first_language + " -> " + self.second_language)
		non_translated = []
		for word in self.embeddings.lang_embeddings[self.first_language]:
			try:
				tr_vec = np.dot(self.embeddings.lang_embeddings[self.first_language][word], self.matrix)
				self.embeddings.lang_embeddings[self.first_language][word] = tr_vec
				self.embeddings.lang_emb_norms[self.first_language][word] =  np.linalg.norm(tr_vec, 2)
			except ValueError: 
				print("Error translating word: " + word)
				non_translated.append(word)
				
		for word in non_translated:
			self.embeddings.lang_embeddings[self.first_language].pop(word, None)
			self.embeddings.lang_emb_norms[self.first_language].pop(word, None)
	


